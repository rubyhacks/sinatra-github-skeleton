require 'rubygems'
require 'bundler'
Bundler.setup

require 'sinatra/base'
require 'sinatra_auth_github'
require 'inifile'

module Demo
  class App < Sinatra::Base
    enable :sessions

    config = IniFile.new( :filename => 'config.ini', :parameter => '=' )

    set :github_options, {
      :scopes    => "user",
      :client_id => config['oauth']['client_id'],
      :secret    => config['oauth']['client_secret'],
    }

    register Sinatra::Auth::Github

    get '/' do
      authenticate!
      "Hello there, #{github_user.login}!"
    end

    get '/logout' do
      logout!
    end
  end
end

# Demo::App.run!
